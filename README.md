Ubuntu Desktop for EC2
======================

This repository contains the configuration for ubuntu desktop running on EC2.

All configuration is applied using the open source configuration managmenet solution 
cfengine, see http://cfengine.com/

cfengine is built on the so called promises theory. This means that the scrips
are executed over and over again. The installation becomes "self healing this way".
An overview of the result of each run is displayed at the end.

```
cf3> Outcome of version 0.1 (agent-0): Promises observed to be kept 64%, Promises repaired 32%, Promises not repaired 4%
cf3> Estimated system complexity as touched objects = 18, for 185 promises
cf3>  -> Writing last-seen observations
cf3>  -> Keyring is empty
cf3>  -> No lock purging scheduled
```

Connecting to the desktop is performed using NX from Nomachine.


Ubuntu
------

login to the server and install cfengine and git:

```
# login
ssh -i server_key.pem ubuntu@XXX.XXX.XXX.XXX

# Install git and cfengine
sudo apt-get install -y cfengine3 git
```

Then this repository needs to be cloned using git:

```
cd /etc/cfengine3
sudo git clone git://github.com/colmsjo/ubuntu_desktop.git .
sudo chmod -R og=r /etc/cfengine3/roles
```

Update the cfengine configuration:

```
# Enable appropriate daemons, typically RUN_CFEXECD=1 and CFEXECD_ARGS="--define phonegap"
sudo nano /etc/default/cfengine3

# Start cfengine
sudo /etc/init.d/cfengine3 restart
```

Only used for development and testing:

```
# Run cf-engine manually
sudo sh -c "cf-agent --verbose --define phonegap> cfengine-log.txt" && more cfengine-log.txt
```
 
### Troubleshooting 
 
The cf-agent job sometimes hangs on the first run. Do a `ps -ef` and look for the 
aptitude process and then `kill` the job.  


Example:

```
ps -ef
...
root     28843   251  0 14:45 ?        00:00:00 /sbin/udevd --daemon
root     28854     1  0 14:45 ?        00:00:00 /usr/sbin/cupsd -F
root     31253  4284  0 14:46 ?        00:00:00 sh -c /usr/bin/aptitude --assume-yes install unzip mutt s3cmd git 
root     31254 31253  0 14:46 ?        00:00:04 /usr/bin/aptitude --assume-yes install unzip mutt s3cmd git


sudo kill 31253

# You can do a reboot, just to make sure there are no processes still running
sudo reboot

# Run the job again, now it should work
sudo sh -c "cf-agent --verbose --define phonegap> cfengine-log.txt" && more cfengine-log.txt
```
 
 
#### NX
 
The NX installation log files can be found here: /usr/NX/var/log/install
 
 
Connect to the desktop
----------------------

The password for the ubuntu user needs to be set. I have soo far not been able to
login with only the provate key (which really should be possible).

```
sudo passwd ubuntu
# enter the password to use
```

Download NX Player: http://www.nomachine.com/download.php

Settings:

 * IP: the server IP
 * Port: 22
 * Use nomachine login 
  * Login: ubuntu
  * Password: whatever-you-entered-above

Start a GNOME Session. The other window handlers are not setup.



Dependencies that needs to be installed manually
------------------------------------------------

The Android SDK only supports the Oracle Java. This needs to be installed manually:

 * http://www.oracle.com/technetwork/java/javase/downloads/jdk6u38-downloads-1877406.html


Connect to the server and login as ubuntu:

```
cd /home/ubuntu/Downloads
sudo mkdir /usr/lib/jvm
sudo mv jdk-6u38-linux-x64.bin  /usr/lib/jvm
cd /usr/lib/jvm
sudo chmod +x jdk-6u38-linux-x64.bin 
sudo ./jdk-6u38-linux-x64.bin
```

The Oracle JVM needs to be the default JVM:

* http://askubuntu.com/questions/67909/how-do-i-install-oracle-jdk-6

Run this from a terminal within the NX Player (a X window is opened):

```
sudo wget http://webupd8.googlecode.com/files/update-java-0.5b
sudo chmod +x update-java-0.5b
sudo ./update-java-0.5b


# Check that the right java is setup
ubuntu@ip-10-239-94-144:~$ javac -version
javac 1.6.0_38
```


Setup ADT
---------

ADT also needs to be installed manually:

 * http://developer.android.com/sdk/installing/installing-adt.html


Within a terminal window in NX player:

```
eclipse
```


Postfix configuration
---------------------

The following needs to be performed manually in order to make it possible to send mails from other hosts than localhost

```
nano /etc/postfix/main.cf


...
smtpd_use_tls=no
...

mynetworks = 10.0.0.0/8 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
...


```

