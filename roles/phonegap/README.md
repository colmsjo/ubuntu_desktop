

From the Getting Started with Android guide:

Install SDK + Cordova:

1. Download and install Eclipse Classic
1. Download and install Android SDK
1. Download and install ADT Plugin
1. Download the latest copy of Cordova and extract its contents. We will be working with the Android directory.


Links:

* http://docs.phonegap.com/en/2.3.0/guide_getting-started_android_index.md.html#Getting%20Started%20with%20Android
* https://help.ubuntu.com/community/EclipseIDE
